//
//  ItuParseTests.swift
//  ItuParseTests
//
//  Created by Константин Паньков on 18/04/2019.
//  Copyright © 2019 kpankov.com. All rights reserved.
//

import XCTest
@testable import easyHamLogDesktop

class ItuParseTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let parser: ItuParser = ItuParser()
        parser.readBaseFromFile(fileName: "ItuPrefixTable")
        
        var callsign = "R2APN"
        var result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "Russia", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "A3GX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "Tonga", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "A4DX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "Oman", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "A5ADX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "Bhutan", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "A6BDX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "United Arab Emirates", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "B0DX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "People's Republic of China", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "B9FX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "People's Republic of China", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "BA0DX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "People's Republic of China", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "BL0DX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "People's Republic of China", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "BMDX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "Taiwan", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
        callsign = "BR0DX"
        result = parser.getCountryFromCallsign(callsign: callsign)
        XCTAssert(result.name == "People's Republic of China", "Wrong Country!")
        print("Result for \(callsign): \(result.name), \(result.code), \(result.flag)")
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
