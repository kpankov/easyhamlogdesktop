//
//  ItuParser.swift
//  easyHamLogDesktop
//
//  Created by Константин Паньков on 18/04/2019.
//  Copyright © 2019 kpankov.com. All rights reserved.
//

import Foundation

class ItuParser {
    
    struct ItuPrefixTable: Codable {
        let countries: [Country]
        
        enum CodingKeys : String, CodingKey {
            case countries = "countries"
        }
    }
    
    struct Country: Codable {
        let name: String
        let code: String
        let flag: String
        let prefix: String
    }
    
    var ituPrefixTableData: Data
    
    init() {
        self.ituPrefixTableData = Data.init()
    }
    
    func readBaseFromFile(fileName: String) {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                self.ituPrefixTableData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            } catch {
                // handle error
            }
        }
    }
    
    func getCountryFromCallsign(callsign: String) -> (name: String, code: String, flag: String) {
        
        guard let countries = try? JSONDecoder().decode(ItuPrefixTable.self, from: ituPrefixTableData).countries else {
            print("Error: Couldn't decode data into JSON")
            return ("Unknown", "XX", "🏴‍☠️")
        }
        
        let range = NSRange(location: 0, length: callsign.utf16.count)
        
        for country in countries {
            if try! NSRegularExpression(pattern: country.prefix).firstMatch(in: callsign, options: [], range: range) != nil {
                return (country.name, country.code, country.flag)
            }
        }
        
        return ("Unknown", "XX", "🏴‍☠️")
    }
    
}
