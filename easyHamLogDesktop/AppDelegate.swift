//
//  AppDelegate.swift
//  easyHamLogDesktop
//
//  Created by Константин Паньков on 18/04/2019.
//  Copyright © 2019 kpankov.com. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

